---
title: 'Titulo: PHP Moderno e Seus Frameworks'
author: Regis Tomkiel/regis@doseextra.com
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---

**Data:** 13/06/2020     
**Participantes:** Regis Tomkiel, Mateus Roveda  
**Convidados:** Igor Santos e Gustavo Fraga  

### Biografias dos convidados:  

#### Igor do Santos:   
 + Desenvolvedor Full Stack na Appmax
 + Coordenador da comunidade PHPRS
 + Palestrante e Escritor
 + Criador de Elephpants
 + Apaixonado por Games e um bom Nerd ;)

 **Links do convidado:**  
 - Twitter: https://twitter.com/IgorSantoos17     
 - Blog: https://medium.com/@IgorSantos17
 - GitHub: https://github.com/IgorSantos17  
 - SpeakerDeck: https://speakerdeck.com/igorsantos
 - Linkedin: https://www.linkedin.com/in/igorsantoos/  



#### Gustavo Fraga:
 + Software Developer
 + Graduado em Análise e Desenvolvimento de Sistemas pela FADERGS
 + iMaster Certified Professional
 + Evangelista PHP
 + Membro da comunidade de desenvolvedores PHP do Rio Grande do Sul - PHPRS

 **Links do convidado:** 
 - Twitter: https://twitter.com/lagraga93    
 - Blog: https://blog.lafraga.me
 - GitHub: https://github.com/lagraga93  
 - SpeakerDeck: https://speakerdeck.com/lagraga93
 - Linkedin: https://www.linkedin.com/in/lagraga93
 - iMasters: https://imasters.com.br/perfil/gustavofraga



#### Short Description:

PHP é uma das linguagem mais populares e utilizadas da atualidade. Sua popularidade que perdura à anos também é motivo de polêmica, com desenvolvedores JavaScript contestando a hegemonia e decretando sua morte nas redes sociais.  

A liguagem é responsável por manter  ao menos 70% da internet e inúmeras ferramentas são mantidas com a linguagem.
Um ótimo exemplo é o onipresente WordPress (obs) e o framework Laravel.
Hoje é dificíl de imaginar o desenvolvimento Web sem ao menos cogitar o PHP.
Vamos conversar sobre o PHP moderno e o que vem mudando desde as versões "clássicas", como o 5.6 que ainda é suportado em muitas hospedagens, mas que já possui alguns anos de estrada.


#### Episódio:
* **Abertura:**
  
    + Introdução:
        - Apresentação hosts:
        - Apresentação dos convidados (!Ver Links do convidado e Bio)
        - Gancho para os convidados: (Por que PHP ?)
    + Mensagens:  
        - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
        - Links:   
            - Facebook:   
            - Spotify:  
            - Player.fm:  
            - Castbox:  
            - Youtube: (TomkielTV)  
            - Twitch: (Tomkiel)  
            - MEETECH Bento Gonçalves: https://www.facebook.com/groups/MEETECH/     
* **Pauta:**
  
    + Início do tema: (!Ver Short Description)  
    
    + Por que PHP em 2020?  
    
    + O que mudou no PHP nos últimos anos?    
    
    + Qual o ambiente de desenvolvimento ideal para desenvolver em PHP?    
    
    + PHP é uma linguagem para quem está iniciando?   

    + O que é um framework PHP?  
    
    + É melhor começar com PHP puro ou com framework?
    
    + Qual a opinião sobre o Laravel? (Obs) 
    
    + Devo me preocupar com performance?  
    
    + O que seria um erro recorrente no desenvolvimento de projetos?   
    
    + Onde eu devo procurar material sobre a linguagem? (Recomendações)   
    
    + O que esperar do futuro do PHP?   
    
      
    
* **Finalização:**  
  
    + Agradecimentos:  
    + Recomendações dos convidados:   
        - Uma música:    
        - Um filme:  
        - Um livro:  
        - Uma dica livre: ( Podcast, site e etc )    


#### Fontes:  
* [https://pt.wikipedia.org/wiki/PHP](https://pt.wikipedia.org/wiki/PHP)  

* [PHP: Do Jeito Certo](http://br.phptherightway.com/)  

* [https://www.php.net](https://www.php.net/manual/pt_BR/intro-whatis.php)  

* [https://www.hostinger.com.br/tutoriais/o-que-e-php-guia-basico/](https://www.hostinger.com.br/tutoriais/o-que-e-php-guia-basico/)    

* [https://www.w3schools.com/Php/default.asp](https://www.w3schools.com/Php/default.asp)

* [https://kinsta.com/pt/blog/o-php-morreu/](https://kinsta.com/pt/blog/o-php-morreu/)

  
