---
title: "Compilado de Dicas para Home Office para KeyWorks"
author: "Regis Tomkiel <regis@doseextra.com>"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---


### Mantenha o Ambiente limpo e organizado
* Um ambiente organizado mantém o foco e gera conforto ao executar as tarefas
diárias.  
* Organização ajuda na hora de focar nos objetivos  
* Evite realizar refeições enquanto trabalha  
* Crie um cronograma de limpeza e organização  
* Crie um local de trabalho  
* Estabeleça um local apenas para o trabalho   
* Evite trabalhar em locais de descanso ou lazer  
* Mantenha apenas programas e APPs relacionados às tarefas em execução   
* Um ambiente bem ventilado e com boa iluminação traz maior produtividade e bem
estar  
* Reúna tudo que você precisa para executar suas tarefas em local de fácil acesso

### Reeduque as pessoas ao seu redor
* Informe as pessoas de sua família sobre sua rotina
* Dedique um tempo para demonstrar como você trabalha, mesmo que de casa

### Crie uma rotina
* Banho ao acordar  
* Fazer o café  
* Arrumar a cama   
* Limpar a mesa/escrivaninha   
* Colocar uma música (aqui entra outro tópico)   
* Vista-se para o trabalho (Evite roupas que você não teria coragem de usar no trabalho em frente seus colegas)   
* Cuide de sua aparência (Quando você se arruma, seu cérebro se prepara para a
atividade que desempenhará)   
* Faça o intervalo de almoço   
* Mantenha um cronograma de horas trabalhadas com início e fim   
* Agende-se consigo mesmo, planeje com um caderno (todoist, calendário). Qualquer
item, mas planeje pela manhã ou no fim do dia anterior.   
* Desative as notificações  
* Informe aos demais membros da família qual o seu horário de trabalho  
* Tenha uma lista de tarefas  
* Estabeleça uma lista de prioridades   
* Crie metas diárias   
* Use ferramentas para ajudar com as metas e entregas   
* Seja autocrítico de seu desempenho no final do dia (Localize o que está
atrapalhando seu desempenho)  
* Pare de contar horas, e comece a contar resultados, o valor que uma tarefa concluída
tem é muito maior que a quantidade de horas trabalhadas.   

### Técnica de pomodoro
* Crie rotinas de trabalhar um período(~30-40min)   
* Descanse um período pequeno(~3-5min)   
* A cada alguns ciclos de (30 ativo+5 parado) tire uma pausa longa(20min)   
* Mantenha comunicação recorrente com os demais membros do time  
* Converse com os demais membros do time com certa frequência   
* Não fique com dúvidas   
* Estabeleça horários para conversas recorrentes.   
* Jogue consigo mesmo, aposte e lhe propicie recompensas, vou terminar isso aqui e vou ali fazer um
café   
* Hipnose, quem gosta não é para todos, é possível estudar os gatilhos do transe da hipnose para
entrar em foco, cuidado ao brincar com sua mente estude e se possível procure hipnólogos
terapêuticos para ajudar  
* Não se cobre tanto agora seu maior inimigo é você mesmo, agora é o momento de perceber
que não é o amigo conversando, não é o ambiente, tudo na sua volta e sua responsabilidade
* Você não é um coocriador e sim um criador, antes parte da responsabilidade era sua, agora  
* Ela é toda sua, então se permita errar, se permita não ser tão bom...  
* Cada dia é único.  
* Cuide horários e alimentação, almoce igual antes longe do PC de preferência, coma em uma
mesa diferente da do trabalho. Fast-food é bom, entregam rápido e é melhor que fazer, mas
isso vai destruir tua motivação com o tempo. 
* Cuide do combustível do seu corpo.  
* Cozinhe! Além de saudável é terapêutico   
* Ler e-mail também é trabalho, responder no chat também é trabalho, reuniões também são
trabalho, você não é medido somente pelo código que entrega e tarefa executada, você ainda
é um ser humano mas cuidado para não usar isso como desculpa para não entregar nada   
* Ofereça ajuda!   

### Saúde e Bem Estar
* Faça alongamentos   
* Faça pausas e mexa-se um pouco  
* Cuide de sua postura  
* Evite permanecer muito tempo em posturas desconfortáveis   
* Beba água (Café também)   
* Não trabalhe em ambiente escuro ou com baixa iluminação!   
* Use livros sob monitor ou laptop, evitando postura ruim    




***Dicas criadas por [Régis Tomkiel](hhtps://twitter.com/tomtomkiel) e [Perceu Bertoletti](https://twitter.com/perceulo) para a empresa [KeyWorks Software](https://www.keyworks.com.br/)***   

**Fique a vontade para submeter atualizações**  
