---
title: "Trabalho Home Office (ou Remoto): Experiências individuais"
author: "Regis Tomkiel<regis@doseextra.com>"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document

---

**Data:** 06/04/2020     
**Participantes:** Regis Tomkiel   
**Convidados:** Cesar Brod, Kleiton Ramil, Kledir Ramil, Gilmara Castro, Mônica Chiesa e Rodrigo Brod

#### **Biografia dos convidados:**  

* ***Kleiton Ramil e Kledir Ramil:***
  + Músicos, escritores, engenheiros formados pela UFRGS com trabalhos icônicos que levam a cultura gaúcha para o Brasil e a cultura brasileira para o mundo.  
    + **Links do convidado:**
      - Twitter: https://twitter.com/kleitonekledir
      - Instagram - https://www.instagram.com/kleitonekledir/
      - Site: http://kleitonekledir.com.br
      - Facebook: https://www.facebook.com/kleitonekledir/
        &nbsp;
* ***Cesar Brod:***
  + Diretor de relacionamento com comunidades do Linux Professional Institute e dinossauro do HomeOffice.
    + **Links do convidado:**
      - Twitter: https://twitter.com/cesarbrod     
      - Blog: http://cesar.brod.com.br 
      - Site: https://lpi.org
      - GitHub: https://github.com/cesarbrod  
      - Gitlab: https://gitlab.com/cesarbrod
      - Linkedin: https://www.linkedin.com/in/cesarbrod/
      - Facebook: https://www.facebook.com/cesarbrod/
        &nbsp;
* **Gilmara Castro:**
  * Desenvolvedora FullStack e consultora agile na Sysvale SoftGroup.
    * **Links do convidado:**
      * Twitter: https://twitter.com/gilmaracastro20
      * Facebook: https://facebook.com/gilmara.castro.967
      * Linkedin: https://www.linkedin.com/in/gilmara-castro/
      * Instagram: https://www.instagram.com/gilmara_castro/
* **Mônica Chiesa:** 
  * Agente de registro, experimentando pela primeira vez o Home Office.
    * **Links do convidado:**
      * Facebook: https://www.facebook.com/monica.chiesa.564
      * Linkedin: https://www.linkedin.com/in/monicachiesa/
      * Instagram: https://www.instagram.com/monica_chiesa/
* **Rodrigo Brod:**
  * Coordenador de curso de graduação de design e sócio do estúdio Frente.
    * **Links do convidado:**
      - Twitter: https://twitter.com/rodrigobrod
      - Site: https://frente.cc
      - Blog: http://r.brod.com.br 
      - Linkedin: https://www.linkedin.com/in/rbrod/
      - Facebook: https://www.facebook.com/rodrigo.brod/
      - https://medium.com/@rodrigobrod
        &nbsp;



#### Short Description:

Em mais uma edição do Opencast sobre Home Office trazemos agora mais um povo com diversos níveis de experiência, desde quem já trabalha remotamente a partir do final dos anos 1980 até quem está tendo essa experiência pela primeira vez, gente da área de TI e gente que só tem a TI como ferramenta mesmo. Participarão desse Opencast:.  (!ver Participantes).
...   


#### Episódio:

* **Abertura:**
  + Introdução:
    - Boas-vindas:
    - Apresentação do convidado (!Ver Links do convidado e Bio)
    - Apresentação hosts:
  + Mensagens:  
    - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
    - Links:      
      - Spotify:
      - iTunes Podcast:
      - Youtube: (TomkielTV)  
      - Twitch: (Tomkiel)     
        &nbsp;  
* **Pauta:**
  + Início do tema: (!Ver Short Description 2)  
  + 
    &nbsp;
* **Finalização:**  
  + Agradecimentos:  
  + Recomendações do convidado:   
    - Uma música:  
    - Um filme ou série:
    - Uma dica livre: (Livro, podcast, site e etc)  

#### Fontes:  

***Opencast podcast 2020***
