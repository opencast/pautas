---
title: 'WordPress para agencias com Mateus Ávila'
author: Regis Tomkiel/regis@doseextra.com
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document

---

**Data:** 31/03/2020   
**Participantes:** Regis Tomkiel, Perceu Bertoletti e Camilo Cunha  
**Convidado:** Mateus Ávila  
**Biografia do convidado:**  

Freelancer front-end com experiência de 10 anos desenvolvendo soluções responsivas em WordPress para clientes do Brasil e no exterior.   

Experiência na utilização das seguintes tecnologias:  

* Stylus  
* VueJS  
* JQuery  
* HTML5  
* Gulp  
* e mais..  

**Links do convidado:**  

* Twitter: https://twitter.com/mavisidoro  
* Site: https://mateusavila.com.br  
* Blog: https://mateusavila.com.br/pt/blog/  
* GitHub: https://github.com/mateusavila  
* Linkedin: https://www.linkedin.com/in/mateusavilaisidoro/
 
#### Short Description:
WordPress é um gerenciador de conteúdo com 16 anos de existência, desenvolvido em PHP, JavaScript e CSS. Por padrão utiliza banco de dados MySQL ou MariaDB.  
É utilizado por aproximadamente 62% da ***internet*** e é mantido comercialmente pela Automattic através do Wordpress.com e pela Fundação WordPress com a edição GPLv2. 
A edição Open Source do projeto é disponibilizada através do site [wordpress.org](wordpress.org), podendo ser baixada e instalada sem qualquer custo.  
O projeto WordPress nasceu de um fork do extinto b2/cafelog, nas mãos de [Ryan Boren](https://twitter.com/rboren) e [Matthew Mullenweg](https://twitter.com/photomatt) e logo foi ganhando a atenção de diversos desenvolvedores, principalmente devido sua fácil configuração e instalação. Outro ponto positivo, está relacionado com a linguagem utilizada no desenvolvimento de seu core e extensões, o famoso PHP.  
Atualmente o WordPress está na versão **5.3.2** e pode ser utilizado nas principais soluções de hospedagem do mercado.


#### Episódio:
* **Abertura:**
    + Introdução:
        - Apresentação hosts:
        - Apresentação do convidado (!Ver Links do convidado e Bio)
        - Gancho para convidado: (Por que ser desenvolvedor?)
    + Mensagens:  
        - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
        - Links:   
            - Facebook:   
            - Spotify:  
            - Player.fm:  
            - Castbox:  
            - Youtube: (TomkielTV)  
            - Twitch: (Tomkiel)  
            - MEETECH Bento Gonçalves: https://www.facebook.com/groups/MEETECH/     
* **Pauta:**
    + Início do tema: (!Ver Short Description)  
    + O que você vê de tão positivo no WordPress?  
    + Quais as vantagens do WordPress ao CMS proprietário da agência?  
    + O que precisamos saber antes de migrar para o WordPress?
    + O que você recomenda para quem é desenvolvedor e pretende criar sites com a ferramenta?
    + Por que o WordPress é tão poderoso?
    + Fale sobre o ambiente ideal de hospedagem:
    + Dicas de Plugins:
    + WordPress e SEO. O que devemos fazer?
    + Quais os principais erros no desenvolvimento de sites?

* **Finalização:**  
    + Agradecimentos:  
    + Recomendações do convidado:   
        - Uma música:  
        - Um filme ou série:
        - Uma dica livre: (Livro, podcast, site e etc)  


#### Fontes:  
* [https://kinsta.com/pt/wordpress-quota-mercado](https://kinsta.com/pt/wordpress-quota-mercado/)  
* [https://pt.wikipedia.org/wiki/WordPress](https://pt.wikipedia.org/wiki/WordPress)  
* [https://wordpress.com](https://wordpress.com/)  
* [https://wordpress.org](https://wordpress.org)  
* [https://pt.wikipedia.org/wiki/Automattic](https://pt.wikipedia.org/wiki/Automattic)   
  
  
    
***Opencast podcast 2020***
