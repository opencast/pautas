---
title: "Dica de Games da Steam para todo mundo"
author: "Regis Tomkiel<regis@doseextra.com>"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---

**Data:** 06/04/2020     
**Participantes:** Regis Tomkiel, Perceu Bertoletti e Mateus Roveda   
**Convidados:** Jonatas Chagas e Igor Santos

#### **Biografia dos convidados:**  

* ***Jonatas Chagas:***
    + Escrever bio  
        + **Links do convidado:**
            - Twitter: https://twitter.com/  
            - Site: https://    
            - Blog: https://    
            - GitHub: https://github.com/    
            - Linkedin: https://www.linkedin.com/in//  
&nbsp;
* ***Igor Santos:***
    + Software Developer na DBC Company
    + Coordenador da comunidade PHPRS
    + Palestrante e Escritor
    + Criador de Elephpants
    + Apaixonado por Games e um bom Nerd ;)
        + **Links do convidado:**
            - Twitter: https://twitter.com/IgorSantoos17     
            - Blog: https://medium.com/@IgorSantos17
            - GitHub: https://github.com/IgorSantos17  
            - SpeakerDeck: https://speakerdeck.com/igorsantos
            - Linkedin: https://www.linkedin.com/in/igorsantoos/  

#### Short Description:
Um bate-papo discontraído sobre indicações de games da Steam.   
Hoje a conversa é sobre jogos baratinhos ou free to play. Àqueles jogos que podem ser jogados por todo mundo, sem precisar de um PC da nasa ou vender um rim para comprar.  
...   

#### Episódio:

* **Abertura:**
    + Introdução:
        - Boas-vindas:
        - Apresentação do convidado (!Ver Links do convidado e Bio)
        - Apresentação hosts:
    + Mensagens:  
        - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
        - Links:      
            - Spotify:
            - iTunes Podcast:
            - Youtube: (TomkielTV)  
            - Twitch: (Tomkiel)     
* **Pauta:**
    + Início do tema: (!Ver Short Description)  
    + Indicação de jogos:   
    + 1: Counter Strike: Go (Grátis)  
    + 2: Horizon Chase Turbo
    + 3: Paladins (Grátis)
    + 4: No More Room in Hell (Grátis)
    + 5: Path of Exile (Grátis)
    + 6: Stardew Valley
    + 7: Project Argo (Grátis)
    + 8: Tomb Raider
    + 9: Ruiner
    + 10: Transistor
    + 11: Age of Mythology
    + 12: Limbro
    + 13: Brothers - A Tale of Two Sons
* **Finalização:**  
    + Agradecimentos:  
    + Recomendações do convidado:   
        - Uma música:  
        - Um filme ou série:
        - Uma dica livre: (Livro, podcast, site e etc)  
* **Links úteis:**  
    + https://www.protondb.com/app/266840
    + https://www.protondb.com/app/238960
    + https://itch.io/

#### Fontes:  

***Opencast podcast 2020***
