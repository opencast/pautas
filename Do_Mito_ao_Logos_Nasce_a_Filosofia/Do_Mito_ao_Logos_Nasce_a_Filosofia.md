---
title: "Do Mito ao Lógos: Nasce a Filosofia"
author: "Regis Tomkiel<regis@doseextra.com>"
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document

---

**Data:** 06/05/2020     
**Participantes:** Regis Tomkiel   
**Convidados:** Vicente Marçal

#### **Biografia dos convidados:**  

* ***Vicente Marçal:***
  
  + Desenvolvedor Python apaixonado, criativo e colaborador de código aberto. Um trabalhador incansável e aprendiz rápido obcecado em agregar valor com software de qualidade.
    Professor adjunto do Departamento de Filosofia da UNIR - Universidade Federal de Rondônia. Doutor em Psicologia Social e do Trabalho pelo IP-USP - Instituto de Psicologia da Universidade de São Paulo. 
    Mestre em Filosofia pela FFC-UNESP - Faculdade de Filosofia e Ciências da Universidade Estadual Paulista/Campus Marília. 
    Especialista em Filosofia Moderna e Contemporânea: Aspectos Éticos e Jurídicos pela UEL - Universidade Estadual de Londrina. 
    Licenciado em Filosofia pela UEL - Universidade Estadual de Londrina. Coordenador do GEPEGRA - Grupo de Estudos e Pesquisa em Epistemologia Genética da Região Amazônica (CNPq) do Departamento de Filosofia da UNIR em Porto Velho-RO. 
    Com experiência em Filosofia, com ênfase em Epistemologia, Epistemologia Genética, Teoria do Conhecimento e Lógica.
  
    
  
    + **Links do convidado:**
      - Twitter: https://twitter.com/vicentemarcal
      - Site: https://www.vicentemarcal.com/perfil/    
      - Blog: https://www.vicentemarcal.com/
      - GitHub: https://github.com/Riverfount   
      - Linkedin: https://www.linkedin.com/in/vicentemarcal  
        &nbsp

#### Episódio:

* **Abertura:**
  + Introdução:
    - Boas-vindas:
    - Apresentação do convidado (!Ver Links do convidado e Bio)
    - Apresentação hosts:
  + Mensagens:  
    - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
    - Links:      
      - Spotify:
      - iTunes Podcast:
      - Youtube: (TomkielTV)  
      - Twitch: (Tomkiel)     
* **Pauta:**
  + Início do tema: 
  
    #### Short Description:
  
    **"A filosofia nasceu com o surgimento de uma nova forma de investigar a realidade (\*physis\*)**.  
    O pensamento filosófico representou uma ruptura com a tradição mitológica, que explicava a harmonia da natureza, as estruturas sociais e muitas outras coisas através do simbolismo da mitologia. O pensamento mítico é um recurso comum a todos os povos da antiguidade, mas, ainda assim, era uma forma de racionalidade."  
  
    Na conversa de hoje falaremos sobre filosofia, mais precisamente sobre a transição do mito à filosofia!  ..   
  
  + O que é "mito"?
  
  + Mito é mentira?
  
  + Importância dos mitos na sociedade?*
  
  + O que é Logos?
  
  + Como foi a raptura com o "mito" e o surgimento da Razão Universal ou Logos?
  
  + Como as práticas comerciais e trocas culturais influenciaram no pensamento filosófico?
  
  + Nossa sociedade atual herdou muito da cultura greco-romana e quando olhamos para o passado, costumamos colocar o Mediterrâneo como centro de nossas discussões sobre conhecimento. 
    O que você poderia dizer sobre esse protagonismo cultural?
  
  + O que é filosofia? (Finalizando) 
* **Finalização:**  
  
  + Agradecimentos:  
  + Recomendações do convidado:   
    - Uma música:  
    - Um filme ou série:
    - Uma dica livre: (Livro, podcast, site e etc)  
* **Links úteis:**  
  + https://pt.slideshare.net/joaopaulorodrigues927/do-mito-ao-logos
  + https://castalio.info/episodio-137-vicente-marcal-filosofia-com-python.html

#### Fontes:  

- [https://www.netmundi.org/filosofia/2014/pre-socraticos-do-mito-ao-logos/](https://www.netmundi.org/filosofia/2014/pre-socraticos-do-mito-ao-logos/)
- https://www.youtube.com/watch?v=cvFXArjXnHY
- [http://www.albertosantos.org/A%20passagem%20do%20mito%20ao%20logos.html](http://www.albertosantos.org/A passagem do mito ao logos.html)

***Opencast podcast 2020***