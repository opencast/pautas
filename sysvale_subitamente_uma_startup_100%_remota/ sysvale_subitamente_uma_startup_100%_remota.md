---
title: 'Sysvale, subitamente, uma startup 100% remota!'
author: Regis Tomkiel/regis@doseextra.com
geometry: "left=1cm,right=1cm,top=2cm,bottom=2cm"
output: pdf_document
---

**Data:** 16/06/2020     
**Participantes:** Regis Tomkiel, Mateus Roveda  
**Convidados:** Eugênio Marques e Cesar Brod

### Biografias dos convidados:  

#### Cesar Brod:   
 + Diretor de relacionamento com comunidades do Linux Professional Institute e dinossauro do HomeOffice.

 **Links do convidado:**  
 - Twitter: https://twitter.com/cesarbrod    
 - Blog: http://cesar.brod.com.br
 - Site: https://lpi.org 
 - GitHub: https://github.com/cesarbrod
 - Gitlab: https://gitlab.com/cesarbrod
 - Linkedin: https://www.linkedin.com/in/cesarbrod/
 - Facebook: https://www.facebook.com/cesarbrod/




#### Eugênio Marques:
 +  Empreendedor, CEO da Sysvale Softgroup,  possui experiência nas áreas de: Desenvolvimento de Sistemas, Redes e Banco de dados, entusiasta das tecnologias livres, tendo o Linux Professional Institute como um de seus parceiros, trabalha com metodologias ágeis, discípulo dos Toltecas e grande defensor de que a felicidade seja o caminho e não somente o objetivo.

 **Links do convidado:** 
 - Instagram: https://www.instagram.com/sysvale/
 - Facebook: https://pt-br.facebook.com/sysvale
 - Linkedin: https://www.linkedin.com/company/sysvale/


#### Short Description:

Estamos vivenciando uma das mudanças mais drásticas no modelo de trabalho desde a última Revolução Industrial. Estamos percebendo que é possível trabalhar de casa mantendo boa produtividade e prazos em dia. Quando afirmamos isso, não estamos pensando em um ou dois setores de atividade.

O trabalho remoto não é nenhuma novidade e como já mostramos no episódio 7 da décima temporada, é realidade de algumas profissões há muito tempo. Todavia, dadas as circunstâncias que estamos vivendo desde o começo de 2020, tornou-se uma realidade "forçada" para setores antes inimagináveis. 

No episódio 7 nosso foco foi no ponto de vista pessoal, com dicas de produtividade e concentração. Nele tivemos a oportunidade de conversar com a Gilmara Castro, colaboradora da Sysvale. Hoje o foco é na organização, nas mudanças necessárias e no impacto nas relações de trabalho. Para essa tarefa....

#### Episódio:
* **Abertura:**
  
    + Introdução:
        - Apresentação hosts:
        - Apresentação dos convidados (!Ver Links do convidado e Bio)
        - Gancho para os convidados: (Por que trabalhar com "tecnologia?") 
    + Mensagens:  
        - Acompanhe os podcasts do Opencast (anchor.fm/opencast)
        - Links:   
            - Facebook:   
            - Spotify:  
            - Player.fm:  
            - Castbox:  
            - Youtube: (TomkielTV)  
            - Twitch: (Tomkiel)  
            - MEETECH Bento Gonçalves: https://www.facebook.com/groups/MEETECH/     
* **Pauta:**
  
    + Início do tema: (!Ver Short Description)  
    
    + Nos conte um pouco sobre a Sysvale:  
    
    + Como foi essa mudança súbita para o home office?    
    
    + Como foi a adaptação? Os funcionários aderiram ou teve resistência? 
    
    + Já estava (trabalhavam) nos planos o trabalho home office?    
    
    + Quais os pontos positivos e negativos dessa mudança?
    
    + Como as empresas podem se benificiar do home office?  
    
    + Olhando para o futuro, a Sysvale irá permanecer nesse formato quando tudo isso passar?
    
    + Vocês acham que após essa pandemia, as empresas enxergarão o home office com outros olhos?
    
* **Finalização:**  
  
    + Agradecimentos:  
    + Recomendações dos convidados:   
        - Uma música:    
        - Um filme:  
        - Um livro:  
        - Uma dica livre: ( Podcast, site e etc )    


#### Fontes:  
* [https://pt.wikipedia.org/wiki/Home_office](https://pt.wikipedia.org/wiki/Home_office)  



